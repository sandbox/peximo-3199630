( function() {
  CKEDITOR.plugins.add( 'tableshowborders', {
    init: function( editor ) {
      // Table dialog must be aware of it.
      CKEDITOR.on( 'dialogDefinition', function( ev ) {
        var dialogName = ev.data.name;

        if ( dialogName == 'table' || dialogName == 'tableProperties' ) {
          var dialogDefinition = ev.data.definition,
            infoTab = dialogDefinition.getContents('info');

          var btn = {
            id : 'tableshowborders',
            type : 'checkbox',
            label : 'Show borders',
            'default' : '',
            setup: function(element) {
              this.setValue(element.hasClass('showborders'));
            },
            commit : function(config, element) {
              if (this.getValue()) {
                element.addClass('showborders');
              }
              else {
                element.removeClass('showborders');
              }
            }
          };

          infoTab.elements.push(btn);
        }
      } );
    }
  } );
} )();
