CKEditor Table Show Borders

The built-in CKEditor Table plugin allows to customize tables and cells border appearance using the `style` attribute. But this attribute is removed if the "Limit allowed HTML tags" filter is enabled.
This small plugin allows editors to show/hide the cells borders using a class.
To use the plugin, install the module as usual and allow the class attribute for the table element.
